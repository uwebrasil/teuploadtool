<?php

abstract class Connect
{
    private static $conn = null;
    private static $erro = null;
    /*
    public static function getConnectionLocal()
    {
        self::$erro = null;
        try 
        {
         self::$conn = new PDO('pgsql:host=127.0.0.1;port=5433;dbname=CRM', 'postgres','password');
        }
        catch (PDOException $e)
        { 
            self::$conn = null; 
            self::$erro = $e->getMessage();
        }
        return self::$conn;
    }
    */
    public static function getConnectionServerIntern()
    {
        self::$erro = null;
        try 
        {
         self::$conn = new PDO('pgsql:host=192.168.25.254;port=5432;dbname=DBCRM', 'postgres','postgres');
        }
        catch (PDOException $e)
        { 
            self::$conn = null; 
            self::$erro = $e->getMessage();
        }
        return self::$conn;
    }

    public static function getConnectionServerExtern()
    {
        self::$erro = null;
        try 
        {
         self::$conn = new PDO('pgsql:host=25.96.50.85;port=5432;dbname=DBCRM', 'postgres','postgres');               
        }
        catch (PDOException $e)
        { 
            self::$conn = null; 
            self::$erro = $e->getMessage();
        }
        return self::$conn;
    }

    public static function getErro() {
        return self::$erro;
    }
}
?>