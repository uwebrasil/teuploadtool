
<div class="container-fluid bootstrap snippets bootdeys">

<div class="page-headerx d-flex align-items-start" style="height:50px !important;">
 <img src="./img/MAIN_LOGO.png" class="halfsize" style="margin-top:-20px !important;">

  <h2 style="margin-left:270px !important;margin-top:10px;">Upload Tool<?php //echo ini_get('max_execution_time');?></h2>
</div>

<!--
<div class="row">
    <div class="col-md-4 col-sm-6 border">
      <img src="./img/MAIN_LOGO.png" class="halfsize">
    </div>
    <div class="col-md-4 col-sm-6 ">
     <h3>Upload Tool</h3>
    </div>
</div>
-->
<!-- tabela_postos_anp-->
<div class="row">
    <div class="col-md-4 col-sm-6 content-card  d-flex justify-content-center">
        <div class="card-big-shadow">
            <div class="card" data-background="color" data-color="blue" data-radius="none" >
                <div class="content text-left">
                    <h6 class="category">Carregar arquivo XSLX para a tabela</h6>
                    <h4 class="title"><a href="#">tabela_postos_anp</a></h4>
                    <p class="description">

                    <form id="formAnp" action="TEUploadAnp.php">
                        <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input" id="fileAnp" name="fileAnp" accept=".xlsx">
                        <label class="custom-file-label" for="fileAnp">Escolher Arquivo</label>
                        </div>
                        <div id="loaderAnp" class="loader"></div>
                        <p id="statusAnp"></p>
                        <p class="stopwatch" id="stopwatch">
                            <!-- stopwatch goes here -->
                        </p>
                        <div class="mt-3">
                        <button type="submit" class="btn btn-outline-dark btn-sm">Submit</button>
                        </div>
                    </form>

                    </p>
                </div>
            </div> <!-- end card -->
        </div>
    </div>




    <!-- FORM -->
    <div class="col-md-4 col-sm-6 content-card  d-flex justify-content-center">
        <div class="card-big-shadow">
            <div class="card" data-background="color" data-color="green" data-radius="none" style="margin-top:-0%;">
                <div class="content text-left">
                    <h6 class="category">Análise Banco de Dados</h6>
                    <!--<h4 class="title"><a href="#">Yellow Card</a></h4>-->
                    <p class="description">

                    <form method="POST" id="form_filtro" name="form_filtro" action="TEApiTest.php">

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">CNPJ</span>
                        </div>
                        <input type="text" id="cnpj" name="cnpj" class="form-control">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Estado</span>
                        </div>
                        <input type="text" id="estado" name="estado" class="form-control">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Cidade</span>
                        </div>
                        <input type="text" id="cidade" name="cidade" class="form-control">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Data</span>
                        </div>
                        <input type="text" id="calendario" name="calendario" class="form-control" autocomplete="off">
                    </div>
                        <!--<button type="button" id="print" class="btn btn-outline-dark btn-sm">Imprimir</button>-->
                        <button type="submit" class="btn btn-outline-dark btn-sm">Submit</button>
                        <button type="button" id="btn_treeview" class="btn btn-outline-dark btn-sm">treeView</button>
                    </form>


                    </p>
                </div>
            </div> <!-- end card -->
        </div>
    </div>

     <!-- tabela_consulta_plus-->
     <div class="col-md-4 col-sm-6 content-card  d-flex justify-content-center">
        <div class="card-big-shadow">
            <div class="card" data-background="color" data-color="blue" data-radius="none">
                <div class="content text-left">
                    <h6 class="category">Carregar arquivo XSLX para a tabela</h6>
                    <h4 class="title"><a href="#">tabela_consulta_plus</a></h4>
                    <p class="description">

                    <form id="formPlus" action="TEUploadPlus.php">
                        <div class="custom-file mb-3">
                        <input type="file" class="custom-file-input" id="filePlus" name="filePlus" accept=".xlsx">
                        <label class="custom-file-label" for="filePlus">Escolher Arquivo</label>
                        </div>
                        <div id="loaderPlus" class="loader"></div>
                        <p id="statusPlus"></p>
                        <p class="stopwatch" id="stopwatchPlus">
                            <!-- stopwatch goes here -->
                        </p>
                        <div class="mt-3">
                        <button type="submit" class="btn btn-outline-dark btn-sm">Submit</button>

                        </div>
                    </form>

                    </p>
                </div>
            </div> <!-- end card -->
        </div>
    </div>
  
    <div id="div_treeview"></div>
    <div id="result" class="row justify-content-center bg-blue flex-grow-1">

    </div>

</div>

<!--
MODAL    
-->
<!--<button id="btn_modal" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="myModalHeader" class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p id="myModalBody">Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
    
  </div>
</div>
<!--
END MODAL    
-->
</div>