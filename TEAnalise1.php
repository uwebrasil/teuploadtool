<?php

// suppress deprecated-warnings
//error_reporting(E_ERROR | E_PARSE);
require_once __DIR__ . '/Connect.php';

$conn = Connect::getConnectionServerExtern();
if ($conn == null) {
    echo Connect::getErro();
    return;
}
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// obter dados de "tabela_consulta_plus" que não tem "CNPJ"
// em "tabela_postos_anp"
$db_consulta_plus = "tabela_consulta_plus";
$db_postos_anp   = "tabela_postos_anp";

$cnpj = $_POST["cnpj"];
$estado = strtoupper($_POST["estado"]);
$cidade = strtoupper($_POST["cidade"]);
$calendario = $_POST["calendario"];
$query =
"select \"CNPJ\", \"Data_Abertura\", \"Razao_Social\", \"Nome_Fantasia\", \"Email1\", 
        \"Email2\", \"Tipo_Email\", \"Telefone1\", \"Telefone2\", \"Porte\", 
        \"CNAE_Principal\", \"CNAE_Sec_1\", \"CNAE_Sec_2\", \"CNAE_Sec_3\", \"CNAE_Sec_4\",
        \"CNAE_Sec_5\", \"CNAE_Sec_6\", \"CNAE_Sec_7\", \"Natureza_Juridica\", \"Capital_Social\",
        \"CEP\", \"UF\", \"Municipio\", \"Bairro\", \"Logradouro\",
        \"Numero\", \"Complemento\", \"Quadro_Socios\"
from " .$db_consulta_plus. "
   where trim(both ' ' from replace(replace( replace(\"CNPJ\",'.',''), '/',''),'-',''))
       not in (select
         trim(both ' ' from replace(replace( replace(\"CNPJ\",'.',''), '/',''),'-',''))
   from " .$db_postos_anp. ")";

$query .= " and (trim(both ' ' from replace(replace( replace(\"CNPJ\",'.',''), '/',''),'-','')) LIKE '%".$cnpj."%')";
$query .= " and   (upper(\"UF\") LIKE '%".$estado."%')";
$query .= " and   (upper(\"Municipio\") LIKE '%".$cidade."%')";
if(!empty($calendario)) {
   $query .= " and   (\"Data_Abertura\" >= '".$calendario."'::date)";
}
$query .= " order by \"CNPJ\"";

$result = $conn->prepare($query);
$result->execute();   
$rows = $result->fetchAll(PDO::FETCH_ASSOC);   

echo "&nbsp;&nbsp;";
echo "Total: ".count($rows);
echo "&nbsp;&nbsp;";
//data
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
$var_DateTime = strtotime('today');
// Caso não queira letras maiúsculas no início de algumas palavras, pode ser usado apenas assim:
echo utf8_encode(strftime('%A, %d, de %B de %Y', $var_DateTime));
// não pega o locale, precisa ser umvariavel .. 
//echo strftime('%A, %d de %B de %Y', strtotime('today'));
echo "&nbsp;&nbsp;";
echo strftime('%R', time());
echo "&nbsp;&nbsp;&nbsp;&nbsp;";
//filtros
echo "Filtros:&nbsp;   CNPJ = "; echo (empty($_POST['cnpj'])) ? '---' : $_POST['cnpj'];
echo ",   Estado = "; echo (empty($_POST['estado'])) ? '---' : $_POST['estado'];
echo ",   Cidade = "; echo(empty($_POST['cidade'])) ? '---' : $_POST['cidade'];
echo ",   Abertura = "; echo (empty($_POST['calendario'])) ? '---' : $_POST['calendario'];
echo "<br />";
//table
echo "<div class=\"tableFixHead table-bordered\">
<table id=\"table_data\" class=\"table w-auto small\">
  <thead>
    <tr>";
echo "<th>#</th>";     
echo "<th>CNPJ</th>"; 
echo "<th>Abertura</th>"; 
echo "<th>Estado</th>"; 
echo "<th>Cidade</th>";
echo "<th>Razao_Social</th>"; 
echo "<th>Nome_Fantasia</th>"; 
echo "<th>Email1</th>"; 
echo "<th>Email2</th>"; 
echo "<th>Tipo_Email</th>"; 
echo "<th>Telefone1</th>"; 
echo "<th>Telefone2</th>"; 
echo "<th>Porte</th>";  
echo "<th>CNAE_Principal</th>"; 
echo "<th>CNAE_Sec_1</th>"; 
echo "<th>CNAE_Sec_2</th>"; 
echo "<th>CNAE_Sec_3</th>"; 
echo "<th>CNAE_Sec_4</th>"; 
echo "<th>CNAE_Sec_5</th>"; 
echo "<th>CNAE_Sec_6</th>"; 
echo "<th>CNAE_Sec_7</th>"; 
echo "<th>Natureza_Juridica</th>"; 
echo "<th>Capital_Social</th>";   
echo "<th>CEP</th>"; 
//echo "<th>UF</th>"; 
//echo "<th>Municipio</th>"; 
echo "<th>Bairro</th>"; 
echo "<th>Logradouro</th>"; 
echo "<th>Numero</th>"; 
echo "<th>Complemento</th>"; 
echo "<th>Quadro_Socios</th>";

echo "<thead>";

echo "<tbody>";
foreach($rows as $key =>$row){
  
   echo "<tr>";
   echo "<td>".($key + 1)."</td>";
   echo "<td class=\"text-nowrap\">".$row['CNPJ']."</td>";
   echo "<td class=\"text-nowrap\">".$row['Data_Abertura']."</td>";
   echo "<td>".$row['UF']."</td>";
   echo "<td>".$row['Municipio']."</td>";
   echo "<td>".$row['Razao_Social']."</td>";
   echo "<td>".$row['Nome_Fantasia']."</td>";
   echo "<td>".$row['Email1']."</td>";
   echo "<td>".$row['Email2']."</td>";
   echo "<td>".$row['Tipo_Email']."</td>";
   echo "<td>".$row['Telefone1']."</td>";
   echo "<td>".$row['Telefone2']."</td>";
   echo "<td>".$row['Porte']."</td>";
   echo "<td>".$row['CNAE_Principal']."</td>";
   echo "<td>".$row['CNAE_Sec_1']."</td>";
   echo "<td>".$row['CNAE_Sec_2']."</td>";
   echo "<td>".$row['CNAE_Sec_3']."</td>";
   echo "<td>".$row['CNAE_Sec_4']."</td>";
   echo "<td>".$row['CNAE_Sec_5']."</td>";
   echo "<td>".$row['CNAE_Sec_6']."</td>";
   echo "<td>".$row['CNAE_Sec_7']."</td>";
   echo "<td>".$row['Natureza_Juridica']."</td>";
   echo "<td>".$row['Capital_Social']."</td>";
   echo "<td>".$row['CEP']."</td>";
   //echo "<td>".$row['UF']."</td>";
   //echo "<td>".$row['Municipio']."</td>";
   echo "<td>".$row['Bairro']."</td>";
   echo "<td>".$row['Logradouro']."</td>";
   echo "<td>".$row['Numero']."</td>";
   echo "<td>".$row['Complemento']."</td>";
   echo "<td>".$row['Quadro_Socios']."</td>";   
   echo "</tr>";
}
echo "</tbody>";

echo "</tbody></table></div>";
?>