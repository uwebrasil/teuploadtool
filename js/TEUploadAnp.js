var myForm = document.getElementById('formAnp');  // Our HTML form's ID
var myFile = document.getElementById('fileAnp');  // Our HTML files' ID
var statusP = document.getElementById('statusAnp');

myForm.onsubmit = function(event) {
    event.preventDefault();
    debugger;
    if (myFile.files.length == 0) {
        statusP.innerHTML = "Nenhum arquivo escolhido!";
        return;
    }
    document.getElementById("fileAnp").disabled = true;
    start();
    statusP.innerHTML = 'Carregando...';
     
    document.getElementById("loaderAnp").style.display="block";
    document.getElementById("stopwatch").style.display="block";  

    // Get the files from the form input
    var files = myFile.files;

    // Create a FormData object
    var formData = new FormData();

    // Select only the first file from the input array
    var file = files[0]; 

    // Check the file type
    //if (!file.type.match('image.*')) {
    //    statusP.innerHTML = 'The file selected is not an image.';
    //    return;
    //}

    // Add the file to the AJAX request
    formData.append('fileAnp', file, file.name);

    // Set up the request
    var xhr = new XMLHttpRequest();

    var root = window.location.host;
    var upFile = '/TEUploadTool/TEUploadAnp.php';
    if (root == "localhost:81") {
        upFile = '/TEUploadTool/TEUploadAnp.php';
    } 

    // Open the connection  
    xhr.open('POST', upFile, true);
   
    //xhr.timeout = 2000; // time in milliseconds
    // Set up a handler for when the task for the request is complete
    xhr.onload = function () {
      if (xhr.status == 200) {
        debugger;         
        statusP.innerHTML = xhr.response;    
        stop();
        document.getElementById("stopwatch").style.display="none";  
        document.getElementById("loaderAnp").style.display="none";
        document.getElementById("fileAnp").disabled = false;    
      }
      else {
        statusP.innerHTML = 'Erro AJAX carregando. Favor, tentar de novo!' + xhr.status + xhr.response;
        //document.getElementById("stopwatch").style.display="none";
        stop();
        document.getElementById("loaderAnp").style.display="none";
        document.getElementById("fileAnp").disabled = false; 
        console.log(xhr);
        debugger;
      }
    xhr.ontimeout = function (e) {
        // XMLHttpRequest timed out. Do something here.
        statusP.innerHTML = 'TIMEOUT';
        alert("Timed out!!!"); 
        console.log("TIMEOUT");
        console.log(e);
    };
    //xhr.onprogress = function(){ //readystate is 3, u can use it for loader, spinners
    //    //console.log('READYSTATE', xhr.readyState);
        //Spinner or Loader
    //    statusP.innerHTML = 'Carregando ...';
    //    debugger;
    //}
    };

    // Send the data.
    xhr.send(formData);
}