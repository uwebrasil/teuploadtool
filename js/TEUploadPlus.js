var myFormPlus = document.getElementById('formPlus');  // Our HTML form's ID
var myFilePlus = document.getElementById('filePlus');  // Our HTML files' ID
var statusPPlus = document.getElementById('statusPlus');

myFormPlus.onsubmit = function(event) {
    event.preventDefault();
    debugger;
    if (myFilePlus.files.length == 0) {
        statusPPlus.innerHTML = "Nenhum arquivo escolhido!";
        return;
    }
    document.getElementById("filePlus").disabled = true;
    startPlus();
    statusPPlus.innerHTML = 'Carregando...';
     
    document.getElementById("loaderPlus").style.display="block";
    document.getElementById("stopwatchPlus").style.display="block";  

    // Get the files from the form input
    var filesPlus = myFilePlus.files;

    // Create a FormData object
    var formDataPlus = new FormData();

    // Select only the first file from the input array
    var filePlus = filesPlus[0]; 

    // Check the file type
    //if (!file.type.match('image.*')) {
    //    statusP.innerHTML = 'The file selected is not an image.';
    //    return;
    //}

    // Add the file to the AJAX request
    formDataPlus.append('filePlus', filePlus, filePlus.name);

    // Set up the request
    var xhrPlus = new XMLHttpRequest();

    var root = window.location.host;
    var upFilePlus = '/TEUploadTool/TEUploadPlus.php';
    if (root == "localhost:81") {
        upFilePlus = '/TEUploadTool/TEUploadPlus.php';
    } 

    // Open the connection  
    xhrPlus.open('POST', upFilePlus, true);
   
    // Set up a handler for when the task for the request is complete
    xhrPlus.onload = function () {
      if (xhrPlus.status == 200) {
        debugger;         
        statusPPlus.innerHTML = xhrPlus.response;    
        stopPlus();
        document.getElementById("stopwatchPlus").style.display="none";  
        document.getElementById("loaderPlus").style.display="none";  
        document.getElementById("filePlus").disabled = false;  
      }
      else {
        statusPPlus.innerHTML = 'Erro AJAX carregando. Favor, tentar de novo!';
        document.getElementById("loaderPlus").style.display="none";
      }     
    }
    // Send the data.
    xhrPlus.send(formDataPlus);
}

$(document).ready(function(){
  $('#upload').click(function(){
      debugger;
      console.log('upload button clicked!')
      var fd = new FormData();    

      var filesPlus = myFilePlus.files;
      var filePlus = filesPlus[0]; 
      
      //formDataPlus.append('filePlus', filePlus, filePlus.name);

      //fd.append( 'userfile', $('#userfile')[0].files[0]);
      fd.append( 'filePlus', $('#filePlus')[0].files[0]);

      
      var root = window.location.host;
      var upFilePlus = '/TEUploadTool/UploadTest.php';
      if (root == "localhost:81") {
          upFilePlus = '/TEUploadTool/UploadTest.php';
      } 

      $.ajax({
        //url: 'upload/do_upload',
        // url: upFilePlus,
        url: 'localhost:81/TEUploadTool/UploadTest.php',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
          debugger;
          console.log('upload success!')
          $('#data').empty();
          $('#data').append(data);
        },
        error: function(e) {
          debugger;
        }
      });
  });
});