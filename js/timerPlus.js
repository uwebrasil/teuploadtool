//function pad ( val ) { return val > 9 ? val : "0" + val; }
let outputPlus = document.getElementById('stopwatchPlus');
let msPlus = 0;
let secPlus = 0;
let minPlus = 0;

function timerPlus() {
    msPlus++;
    if(msPlus >= 100){
        secPlus++
        msPlus = 0
    }
    if(secPlus === 60){
        minPlus++
        secPlus = 0
    }
    if(minPlus === 60){
        msPlus, secPlus, minPlus = 0;
    }

    //Doing some string interpolation
    let milliPlus = msPlus < 10 ? `0`+ msPlus : msPlus;
    let secondsPlus = secPlus < 10 ? `0`+ secPlus : secPlus;
    let minutePlus = minPlus < 10 ? `0` + minPlus : minPlus;

    let timerPlus= `${minutePlus}:${secondsPlus}:${milliPlus}`;
    outputPlus.innerHTML =timerPlus;
};
//Start timer
function startPlus(){
timePlus = setInterval(timerPlus,10);
}
//stop timer
function stopPlus(){
    clearInterval(timePlus)
}
//reset timer
function resetPlus(){
    msPlus = 0;
    secPlus = 0;
    minPlus = 0;
    outputPlus.innerHTML = '00:00:00';
}