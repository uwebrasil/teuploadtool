<?php
/**
  * Вставляет записи из массива в таблицу
  * @param string $tableName - имя таблицы
  * @param array  $data      - массив строк
  * @param string $delimeter - разделитель значений в строках
  * @return bool
  * @throws Exception
  */
  function insertFromCSV($conn, $tableName, $data, $delimeter = '|')
  {
     
      $PGDB = pg_connect("host=25.96.50.85 port=5432 dbname=DBCRM user=postgres password=postgres");
      $result = pg_copy_from($PGDB, $tableName, $data, $delimeter);
      if ($result === false) {
          throw new Exception(pg_last_error());
      }
      return $result;
  }
/**
 * XLS parsing uses php-excel-reader from http://code.google.com/p/php-excel-reader/
 */
// suppress deprecated-warnings
error_reporting(E_ERROR | E_PARSE);
require_once __DIR__ . '/../Connect.php';

$database = "tabela_postos_anp_test";

$conn = Connect::getConnectionServerExtern();
if ($conn == null) {
    echo Connect::getErro();
    return;
}
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

require_once 'php-excel-reader/excel_reader2.php';
require_once 'SpreadsheetReader.php';

function getCNPJList()
{
    global $conn;
    global $database;
    $sth = $conn->prepare("select \"CNPJ\" from " . $database);
    $sth->execute();
    /* Fetch all of the remaining rows in the result set */   
    $result = $sth->fetchAll(PDO::FETCH_COLUMN, 0);
    return ($result);
}

function dateFormat($s)
{
    // treats two cell "Date" formats
    // *DATE
    // visible in Excelsheet : 24/04/2001
    // Excelreader transforms to: 04-24-01 (* means dependent regionally)
    // GERAL: 24/04/2001
    // Postgres Format for date Insert: 2001-01-24
    // yyyy-mm-dd

    /* TEST 
    *DATE
    $str = "04-24-01";
    $pattern = "/[0-1][0-9]\-[0-3][0-9]\-[0-9][0-9]/";
    echo preg_match($pattern, $str);
    GERAL
    $str2 = "27/01/2001";
    $pattern2= "/[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9]/";
    echo preg_match($pattern2, $str2);
    */

    //$str = trim($s);
    //Excel fills up the cell with this ...
    $str = trim($s, ' ' . chr(194) . chr(160));
    // *DATE MM-DD-YY
    $pattern = "/[0-1][0-9]\-[0-3][0-9]\-[0-9][0-9]/";
    $found =  preg_match($pattern, $str);
    if($found == 1) {
     $arr = explode("-", $str);
     $ret = "20" . $arr[2] . "-" . $arr[0] . "-" . $arr[1];
     return $ret;
    }
    // GERAL DD/MM/YYYY
    $pattern= "/[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9]/";
    $found =  preg_match($pattern, $str);
    if($found == 1) {
     $arr = explode("/", $str);
     $ret = "" . $arr[2] . "-" . $arr[1] . "-" . $arr[0];
     return $ret;
    }

    return $str;
}

function secondsToHHMMSS($seconds)
{
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
}

function formatRow($row)
{
    //  1    dt_autorizacao
    // 12    dt_vinculo_bandeira
    $row[1] = dateformat($row[1]);
    $row[12] = dateformat($row[12]);
    return $row;
}


function formatBulk($row)
{
    $s="";
    foreach ($row as $Key => $val) {
        $s .= strval($val) ."|";
    }    
    $s = substr($s, 0, -1);
    $s .= "\n";      
    
    return $s;
}

function readAnp($Filepath)
{
    global $conn;
    global $database;

    date_default_timezone_set('UTC');

    $StartMem = memory_get_usage();

    $ListOfCNPJ = getCNPJList();

    $errors = array();
    $error_count = 0;
    $inserted = 0;
    $updated = 0;
    $sheet_count = 0;    

    $INSERT = array();

    try
    {
        $Spreadsheet = new SpreadsheetReader($Filepath);
        $BaseMem = memory_get_usage();

        $Sheets = $Spreadsheet->Sheets();

        if ($Sheets[0] != "ANP") {
            return "Planilha \"ANP\" não encontrado. 
                   Arquivo XLSX errado para importar na tabela \"tabela_postos_anp\"! Ação cancelado!";          
        }
        $conn->beginTransaction();
        foreach ($Sheets as $Index => $Name) {
            if ($sheet_count > 0) {
                break;
            } else {
                $sheet_count = $sheet_count + 1;
            }

            $Time = microtime(true);

            $Spreadsheet->ChangeSheet($Index);

            $stmt = $conn->prepare(
                "INSERT INTO " . $database ."
			     (\"Autorizacao_ANP\",\"Dt_Autorizacao\",\"Codigo_Simp\",
				  \"Razao_Social\",\"CNPJ\",\"Endereco\",\"Complemento\",\"Bairro\",
				  \"CEP\",\"UF\",\"Municipio\",\"Bandeira\",\"Dt_Vinculo_Bandeira\"
				 )
				 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
            );

            $sql = "UPDATE " .$database. " SET  
                           \"Autorizacao_ANP\"=?, 
                           \"Dt_Autorizacao\"=?,
                           \"Razao_Social\"=?,
                           \"Codigo_Simp\"=?,
                           \"Endereco\"=?,
                           \"Complemento\"=?,
                           \"Bairro\"=?,
                           \"CEP\"=?,
                           \"UF\"=?,
                           \"Municipio\"=?,
                           \"Bandeira\"=?,
                           \"Dt_Vinculo_Bandeira\"=?                          
                           WHERE \"CNPJ\"=?";
            $stmt_update=$conn->prepare($sql);
            
            foreach ($Spreadsheet as $Key => $Row) {
                if ($Row) {
                    // CNPJ
                    if ($Row[4]) {
                        if ($Row[4] == "CNPJ") {
                            // Column Headers correspondente DataBase Columns
                            //[0] => Autorizacao_ANP
                            //[1] => Dt_Autorizacao         date -> precisa ser formatado
                            //[2] => Codigo_Simp
                            //[3] => Razao_Social
                            //[4] => CNPJ
                            //[5] => Endereco
                            //[6] => Complemento
                            //[7] => Bairro
                            //[8] => CEP
                            //[9] => UF
                            //[10] => Municipio
                            //[11] => Bandeira
                            //[12] => Dt_Vinculo_Bandeira   date -> precisa ser formatado
                        } else {
                            if (in_array($Row[4], $ListOfCNPJ)) {
                                // UPDATE
                                try {
                                 $rowDateFormatted = formatRow($Row);
                                }
                                catch(Exeption $ed) {
                                    echo "Date error";
                                    var_dump($key);
                                    var_dump($cnpj);
                                }

                                $Autorizacao_ANP = $rowDateFormatted[0];
                                $Dt_Autorizacao  = $rowDateFormatted[1];
                                $Codigo_Simp = $rowDateFormatted[2];
                                $Razao_Social = $rowDateFormatted[3];
                                $CNPJ = $rowDateFormatted[4];
                                $Endereco = $rowDateFormatted[5];
                                $Complemento = $rowDateFormatted[6];
                                $Bairro = $rowDateFormatted[7];
                                $CEP = $rowDateFormatted[8];
                                $UF = $rowDateFormatted[9];
                                $Municipio = $rowDateFormatted[10];
                                $Bandeira = $rowDateFormatted[11];
                                $Dt_Vinculo_Bandeira  = $rowDateFormatted[12];

                                $stmt_update->execute([$Autorizacao_ANP,$Dt_Autorizacao,$Codigo_Simp,$Razao_Social,
                                $Endereco,$Complemento,$Bairro,$CEP,$UF,$Municipio,$Bandeira,$Dt_Vinculo_Bandeira,
                                $CNPJ]);

                                $updated = $updated + 1;
                            } else {
                                try {
                                    $rowDateFormatted = formatRow($Row);
                                    $stmt->execute($rowDateFormatted);
                                   $xrow = formatBulk($rowDateFormatted);
                                   $INSERT[] = $xrow;
                                    $inserted = $inserted + 1;
                                } catch (Exception $e) {
                                    $error_count = $error_count + 1;
                                    echo "<br />ERRO: " . $e->getMessage();
                                }
                            }
                        }
                    }
                } else {
                    var_dump($Row);
                    $errors[$error_count] = $Row;
                    $error_count = $error_count + 1;
                }
                $CurrentMem = memory_get_usage();
            }
        }
        $conn->commit();
        //var_dump($INSERT);       
  
        //$tableName = "tabela_postos_anp_test";
        //nsertFromCSV('dummy', $tableName, $INSERT, $delimeter = '|');

        $CurrentMem = memory_get_usage();


        $ret = "<br />Inserted: " . $inserted
        . "<br />Updated: " . $updated
        . "<br />Errors: " . $error_count
        . "<br />Time: " . secondsToHHMMSS((microtime(true) - $Time))
            . "<br />Memory: " . ($CurrentMem - $BaseMem);
    } catch (Exception $E) {
        echo $E->getMessage();
    }
    return $ret;
}
