<?php
/**
 * XLS parsing uses php-excel-reader from http://code.google.com/p/php-excel-reader/
 */
// suppress deprecated-warnings
error_reporting(E_ERROR | E_PARSE);
require_once __DIR__ . '/../Connect.php';

$database = "tabela_consulta_plus_test";
$conn = Connect::getConnectionServerExtern();
if ($conn == null) {
    echo Connect::getErro();
    return;
}
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

require_once 'php-excel-reader/excel_reader2.php';
require_once 'SpreadsheetReader.php';

function getCNPJList()
{
    global $conn;
    global $database;
    $sth = $conn->prepare("select \"CNPJ\" from " . $database);
    $sth->execute();
    /* Fetch all of the remaining rows in the result set */
    $result = $sth->fetchAll(PDO::FETCH_COLUMN, 0);
    return ($result);
}

function dateFormat($s)
{
    // treats two cell "Date" formats
    // *DATE
    // visible in Excelsheet : 24/04/2001
    // Excelreader transforms to: 04-24-01 (* means dependent regionally)
    // GERAL: 24/04/2001
    // Postgres Format for date Insert: 2001-01-24
    // yyyy-mm-dd

    /* TEST
     *DATE
    $str = "04-24-01";
    $pattern = "/[0-1][0-9]\-[0-3][0-9]\-[0-9][0-9]/";
    echo preg_match($pattern, $str);
    GERAL
    $str2 = "27/01/2001";
    $pattern2= "/[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9]/";
    echo preg_match($pattern2, $str2);
     */

    //$str = trim($s);
     //Excel fills up the cell with this ...
     $str = trim($s, ' ' . chr(194) . chr(160));
     
    // *DATE MM-DD-YY
    $pattern = "/[0-1][0-9]\-[0-3][0-9]\-[0-9][0-9]/";
    $found = preg_match($pattern, $str);
    if ($found == 1) {
        $arr = explode("-", $str);
        $ret = "20" . $arr[2] . "-" . $arr[0] . "-" . $arr[1];
        return $ret;
    }
    // GERAL DD/MM/YYYY
    $pattern = "/[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9]/";
    $found = preg_match($pattern, $str);
    if ($found == 1) {
        $arr = explode("/", $str);
        $ret = "" . $arr[2] . "-" . $arr[1] . "-" . $arr[0];
        return $ret;
    }

    return $str;
}

function secondsToHHMMSS($seconds)
{
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
}

function formatRow($row)
{
    //  1    dt_autorizacao
    // 12    dt_vinculo_bandeira
    $row[1] = dateformat($row[1]);
    //$row[12] = dateformat($row[12]);
    return $row;
}
function readPlus($Filepath)
{
    global $conn;
    global $database;

    date_default_timezone_set('UTC');

    $StartMem = memory_get_usage();

    $ListOfCNPJ = getCNPJList();

    $errors = array();
    $error_count = 0;
    $inserted = 0;
    $updated = 0;
    $sheet_count = 0;

    try
    {
        $Spreadsheet = new SpreadsheetReader($Filepath);
        $BaseMem = memory_get_usage();

        $Sheets = $Spreadsheet->Sheets();

        if ($Sheets[0] == "ANP") {
            return "Planilha \"ANP\" encontrado.
                    Arquivo XLSX errado para importar na tabela \"tabela_consulta_plus\"! Ação cancelado!";
        }

        foreach ($Sheets as $Index => $Name) {
            if ($sheet_count > 0) {
                break;
            } else {
                $sheet_count = $sheet_count + 1;
            }

            $Time = microtime(true);

            $Spreadsheet->ChangeSheet($Index);

            $stmt = $conn->prepare(
                "INSERT INTO " . $database . "
			     (\"CNPJ\",\"Data_Abertura\",\"Razao_Social\",
				  \"Nome_Fantasia\",\"Email1\",\"Email2\",\"Tipo_Email\",\"Telefone1\",
				  \"Telefone2\",\"Porte\",\"CNAE_Principal\",\"CNAE_Sec_1\",\"CNAE_Sec_2\",
                  \"CNAE_Sec_3\",\"CNAE_Sec_4\",\"CNAE_Sec_5\",\"CNAE_Sec_6\",\"CNAE_Sec_7\",
                  \"Natureza_Juridica\",\"Capital_Social\",\"CEP\",\"UF\",\"Municipio\",
                  \"Bairro\",\"Logradouro\",\"Numero\",\"Complemento\",\"Quadro_Socios\"
				 )
				 VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            );

            $sql = "UPDATE " . $database . " SET
            \"Data_Abertura\"=?,\"Razao_Social\"=?,
            \"Nome_Fantasia\"=?,\"Email1\"=?,\"Email2\"=?,\"Tipo_Email\"=?,\"Telefone1\"=?,
            \"Telefone2\"=?,\"Porte\"=?,\"CNAE_Principal\"=?,\"CNAE_Sec_1\"=?,\"CNAE_Sec_2\"=?,
            \"CNAE_Sec_3\"=?,\"CNAE_Sec_4\"=?,\"CNAE_Sec_5\"=?,\"CNAE_Sec_6\"=?,\"CNAE_Sec_7\"=?,
            \"Natureza_Juridica\"=?,\"Capital_Social\"=?,\"CEP\"=?,\"UF\"=?,\"Municipio\"=?,
            \"Bairro\"=?,\"Logradouro\"=?,\"Numero\"=?,\"Complemento\"=?,\"Quadro_Socios\"=?
                           WHERE \"CNPJ\"=?";
            $stmt_update = $conn->prepare($sql);

            foreach ($Spreadsheet as $Key => $Row) {
                if ($Row) {
                    // CNPJ
                    if ($Row[0]) {

                        if ($Row[0] == "CNPJ") {
                            // Column Headers correspondente DataBase Columns
                            //[0] => Autorizacao_ANP
                            //[1] => Dt_Autorizacao         date -> precisa ser formatado
                            //[2] => Codigo_Simp
                            //[3] => Razao_Social
                            //[4] => CNPJ
                            //[5] => Endereco
                            //[6] => Complemento
                            //[7] => Bairro
                            //[8] => CEP
                            //[9] => UF
                            //[10] => Municipio
                            //[11] => Bandeira
                            //[12] => Dt_Vinculo_Bandeira   date -> precisa ser formatado
                        } else {
                            if (in_array($Row[0], $ListOfCNPJ)) {
                                // UPDATE
                                $rowDateFormatted = formatRow($Row);
                                $CNPJ = $rowDateFormatted[0];
                                $Data_Abertura = $rowDateFormatted[1];
                                $Razao_Social = $rowDateFormatted[2];
                                $Nome_Fantasia = $rowDateFormatted[3];
                                $Email1 = $rowDateFormatted[4];
                                $Email2 = $rowDateFormatted[5];
                                $Tipo_Email = $rowDateFormatted[6];
                                $Telefone1 = $rowDateFormatted[7];
                                $Telefone2 = $rowDateFormatted[8];
                                $Porte = $rowDateFormatted[9];
                                $CNAE_Principal = $rowDateFormatted[10];
                                $CNAE_Sec1 = $rowDateFormatted[11];
                                $CNAE_Sec2 = $rowDateFormatted[12];
                                $CNAE_Sec3 = $rowDateFormatted[13];
                                $CNAE_Sec4 = $rowDateFormatted[14];
                                $CNAE_Sec5 = $rowDateFormatted[15];
                                $CNAE_Sec6 = $rowDateFormatted[16];
                                $CNAE_Sec7 = $rowDateFormatted[17];
                                $Natureza_Juridica = $rowDateFormatted[18];
                                $Capital_Social = $rowDateFormatted[19];
                                $CEP = $rowDateFormatted[20];
                                $UF = $rowDateFormatted[21];
                                $Municipio = $rowDateFormatted[22];
                                $Bairro = $rowDateFormatted[23];
                                $Logradouro = $rowDateFormatted[24];
                                $Numero = $rowDateFormatted[25];
                                $Complemento = $rowDateFormatted[26];
                                $Quadro_Socios = $rowDateFormatted[27];

                                $stmt_update->execute([
                                    $Data_Abertura, $Razao_Social,
                                    $Nome_Fantasia, $Email1, $Email2, $Tipo_Email, $Telefone1,
                                    $Telefone2, $Porte, $CNAE_Principal, $CNAE_Sec1, $CNAE_Sec2,
                                    $CNAE_Sec3, $CNAE_Sec4, $CNAE_Sec5, $CNAE_Sec6, $CNAE_Sec7,
                                    $Natureza_Juridica, $Capital_Social, $CEP, $UF, $Municipio,
                                    $Bairro, $Logradouro, $Numero, $Complemento, $Quadro_Socios,
                                    $CNPJ]);

                                $updated = $updated + 1;
                            } else {
                                try {
                                    $rowDateFormatted = formatRow($Row);
                                    $stmt->execute($rowDateFormatted);
                                    $inserted = $inserted + 1;
                                } catch (Exception $e) {
                                    $error_count = $error_count + 1;
                                    echo "<br />ERRO: " . $e->getMessage();
                                }
                            }
                        }
                    }
                } else {
                    var_dump($Row);
                    $errors[$error_count] = $Row;
                    $error_count = $error_count + 1;
                }
                $CurrentMem = memory_get_usage();
            }
        }
        $ret = "<br />Inserted: " . $inserted
        . "<br />Updated: " . $updated
        . "<br />Errors: " . $error_count
        . "<br />Time: " . secondsToHHMMSS((microtime(true) - $Time))
            . "<br />Memory: " . ($CurrentMem - $BaseMem);
    } catch (Exception $E) {
        echo $E->getMessage();
    }
    return $ret;
}
